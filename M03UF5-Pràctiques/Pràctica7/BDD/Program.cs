﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using Npgsql;
using System.Linq;
using NpgsqlTypes;

namespace BDD
{
    class Program
    {
        // Main program.
        static void Main()
        {
            List<string> stats = new List<string>() { "abilities", "against_bug", "against_dark", "against_dragon", "against_electric", "against_fairy", "against_fight", "against_fire", "against_flying", "against_ghost", "against_grass", "against_ground", "against_ice", "against_normal", "against_poison", "against_psychic", "against_rock", "against_steel", "against_water", "attack", "base_egg_steps", "base_happiness", "base_total", "capture_rate", "classfication", "defense", "experience_growth", "height_m", "hp", "name", "percentage_male", "pokedex_number", "sp_attack", "sp_defense", "speed", "type1", "type2", "weight_kg", "generation", "is_legendary" };
            NpgsqlConnection con = Connection("programacion");
            string tableName = "pokemon";
            try
            {
                con.Open();
            }
            catch (PostgresException)
            {
                Console.WriteLine("Contraseña incorrecta");
            }
            if (!CheckStateConnection(con)) return;
            CreateTable(con, tableName, stats);
            InsertData(con, tableName, stats);
            List<Pokemon> pokedex = CreatePokemons(con, tableName);
            UpdatePokemon(pokedex, con);
            DeleteLegendaries(con);
            CreateLegendaries(pokedex, con);
            con.Close();
        }
        public static void CreateLegendaries(List<Pokemon> pokedex, NpgsqlConnection con)
        {
            string sqlCreateTable = $"DROP TABLE IF EXISTS Legendarios; CREATE TABLE Legendarios(pokedex_number INT, name VARCHAR(250), generation INT, type1 VARCHAR(250), type2 VARCHAR(250))";
            var command = new NpgsqlCommand(sqlCreateTable, con);
            command.ExecuteNonQuery();

            string sqlInsertInto = $"INSERT INTO Legendarios(pokedex_number,name,generation,type1,type2) VALUES(@pokedex_number,@name,@generation,@type1,@type2)";
            var command2 = new NpgsqlCommand(sqlInsertInto, con);

            foreach (Pokemon pokemon in pokedex)
            {
                if (pokemon.IsLegendary == 1)
                {
                    command2.Parameters.Clear();
                    command2.Parameters.AddWithValue("pokedex_number", NpgsqlDbType.Integer, pokemon.PokedexNum);
                    command2.Parameters.AddWithValue("name", NpgsqlDbType.Varchar, 250, pokemon.Name);
                    command2.Parameters.AddWithValue("generation", NpgsqlDbType.Integer, pokemon.Generation);
                    command2.Parameters.AddWithValue("type1", NpgsqlDbType.Varchar, 250, pokemon.Type1);
                    if (string.IsNullOrEmpty(pokemon.Type2)) command2.Parameters.AddWithValue("type2", NpgsqlDbType.Varchar, 250, "");
                    else command2.Parameters.AddWithValue("type2", NpgsqlDbType.Varchar, 250, pokemon.Type2);
                    command2.Prepare();
                    command2.ExecuteNonQuery();
                }
            }
        }
        public static void UpdatePokemon(List<Pokemon> pokedex, NpgsqlConnection con)
        {

            Console.WriteLine("VAMOS A ACTUALIZAR A CHARMANDER");
            string sqlUpdate = $"UPDATE pokemon SET type1 = @type1 WHERE name = @name;";

            // Creamos los valores que nos interesará actualizar
            Pokemon charmander = new Pokemon("charmander");
            foreach (Pokemon pokemon in pokedex)
            {
                if (pokemon.Name == "Charmander")
                {
                    Console.WriteLine($"{pokemon}\npulsa cualquier botón...");
                    Console.ReadKey();
                    pokemon.Type1 = "fuego";
                    charmander = new Pokemon(pokemon);
                    break; // Una vez encontrado, salimos del bucle
                }
            }

            // Asignamos los parámetros y ejecutamos utilizando iteración
            using var command = new NpgsqlCommand(sqlUpdate, con);
            command.Parameters.AddWithValue("type1", charmander.Type1);
            command.Parameters.AddWithValue("name", charmander.Name);
            command.ExecuteNonQuery();

            Console.WriteLine("Actualizado correctamente...");
            Console.WriteLine(charmander);
        }
        public static void DeleteLegendaries(NpgsqlConnection con)
        {
            Console.WriteLine("ELIMINANDO POKEMONS LEGENDARIOS");
            Console.WriteLine("pulsa cualquier botón...");
            Console.ReadKey();

            string sqlDELETE = $"DELETE FROM pokemon WHERE is_legendary = '1';";

            var command = new NpgsqlCommand(sqlDELETE, con);
            command.ExecuteNonQuery();
            Console.WriteLine("Eliminados correctamente...");
        }

        /// <summary>
        /// Leyendo la base de datos crea una lista de objetos pokemon
        /// </summary>
        /// <param name="con"></param>
        public static List<Pokemon> CreatePokemons(NpgsqlConnection con, string tableName)
        {
            List<Pokemon> listaPokemon = new List<Pokemon>();
            string sql = $"select * from {tableName};";
            using var cmd2 = new NpgsqlCommand(sql, con);
            using NpgsqlDataReader rdr = cmd2.ExecuteReader();

            //Leemos las filas y creamos un pokemon por cada fila
            while (rdr.Read())
            {
                int pokedexNum = Convert.ToInt32(rdr["pokedex_number"]), gen = Convert.ToInt32(rdr["generation"]), legen = Convert.ToInt32(rdr["is_legendary"]);
                string pokeName = TrimString(rdr["name"].ToString()), type1 = TrimString(rdr["type1"].ToString()), type2 = TrimString(rdr["type2"].ToString());
                Pokemon newPokemon = string.IsNullOrEmpty(type2) ? new Pokemon(pokedexNum, pokeName, gen, type1, legen) : new Pokemon(pokedexNum, pokeName, gen, type1, type2, legen);
                listaPokemon.Add(newPokemon);
            }
            //Mostramos todos los pokemon por pantalla.
            foreach (Pokemon pokemon in listaPokemon) Console.WriteLine(pokemon);
            return listaPokemon;
        }

        /// <summary>
        /// Función para quitar todos los espacios de una string.
        /// </summary>
        /// <param name="paraula"></param>
        /// <returns></returns>
        public static string TrimString(string paraula)
        {
            string paraulaTrimmed = string.Concat(paraula.Where(c => !Char.IsWhiteSpace(c)));
            return paraulaTrimmed;
        }
        // Loads the .csv file, cleans it up and adds the values to the SQL table.
        private static void InsertData(NpgsqlConnection con, string tableName, List<string> stats)
        {
            // File path.
            string filePath = @"..\..\..\pokemon.csv";

            // Check if the CSV file exists.
            if (!File.Exists(filePath))
            {
                // Message stating CSV file could not be located.
                Console.WriteLine("Could not locate the CSV file.");
                return;
            }

            // Assign the CSV file to a reader object.
            StreamReader reader = new StreamReader(filePath);
            string[] currentRow;
            string line;

            // Cleans the .CSV file.
            bool firstRow = true;
            while ((line = reader.ReadLine()) != null)
            {
                line = line.Replace("['", "").Replace("']", "");
                currentRow = line.Split(';');
                if (!firstRow) InsertDataPokemon(currentRow, con, tableName, stats);
                firstRow = false;
            }
        }

        // Creates the pokemon table.
        private static void CreateTable(NpgsqlConnection con, string tableName, List<string> stats)
        {
            // Creates the SQL table
            string sql = $"DROP TABLE IF EXISTS {tableName}; CREATE TABLE {tableName} ({string.Join(" CHAR(250), ", stats.ToArray())} CHAR (250))";

            // Sends the create query.
            NpgsqlCommand commmand = new NpgsqlCommand(sql, con);
            commmand.ExecuteNonQuery();
        }

        // Inserts all the values into the pokemon table.
        private static void InsertDataPokemon(string[] pokemonRow, NpgsqlConnection con, string tableName, List<string> stats)
        {
            // Opens a connection and inserts pokemon data.
            string sql = $"INSERT INTO {tableName}({string.Join(",", stats.ToArray())}) VALUES(@{string.Join(",@", stats.ToArray())})";
            NpgsqlCommand cmd = new NpgsqlCommand(sql, con);

            // Adds the values and prepares them.
            for (int i = 0; i < stats.Count; i++) cmd.Parameters.AddWithValue(stats[i], NpgsqlDbType.Varchar, 250, pokemonRow[i]);
            cmd.Prepare();

            // Executes the command and closes the connection.
            cmd.ExecuteNonQuery();
        }

        // Checks the connection state, and returns true/false if open.
        private static bool CheckStateConnection(NpgsqlConnection con)
        {
            if (con.State == ConnectionState.Open)
            {
                Console.WriteLine("Conectado");
                return true;
            }
            return false;
        }

        // Creates a connection and returns it.
        private static NpgsqlConnection Connection(string dbName)
        {
            string cs = $"Host=localhost;Username=postgres;Password=psql;Database={dbName}";
            NpgsqlConnection con = new NpgsqlConnection(cs);
            return con;
        }
    }
}