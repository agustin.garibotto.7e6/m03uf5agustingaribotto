﻿namespace BDD
{
    class Pokemon
    {
        // Atributos

        public int PokedexNum { get; set; }
        public string Name { get; set; }
        public int Generation { get; set; }
        public string Type1 { get; set; }
        public string Type2 { get; set; }
        public int IsLegendary { get; set; }

        // Constructores
        public Pokemon(int pokedexNum, string name, int generation, string type1, string type2, int isLegendary)
        {
            PokedexNum = pokedexNum;
            Name = name;
            Generation = generation;
            Type1 = type1;
            Type2 = type2;
            IsLegendary = isLegendary;
        }
        public Pokemon(Pokemon copy)
        {
            PokedexNum = copy.PokedexNum;
            Name = copy.Name;
            Generation = copy.Generation;
            Type1 = copy.Type1;
            Type2 = copy.Type2;
            IsLegendary = copy.IsLegendary;
        }
        public Pokemon(int pokedexNum, string name, int generation, string type1, int isLegendary) : this(pokedexNum,name,generation,type1,null,isLegendary){}
        public Pokemon(string name) {Name = name;}
        // Metodos
        public override string ToString()
        {

            string isLegendary = IsLegendary == 1 ? "és legendari" : "no es Legendari";
            string pokemon = Type2 == null ? $"[{PokedexNum}]: {Name} [Generación]: {Generation} [Tipos]: ({Type1}) - {isLegendary}" : $"[{PokedexNum}]: {Name} [Generación]: {Generation} [Tipos]: ({Type1}), ({Type2}) - {isLegendary}";
            return pokemon;
        }
    }
}
