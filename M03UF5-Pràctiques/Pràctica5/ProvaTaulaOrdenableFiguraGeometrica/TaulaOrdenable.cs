﻿using System;
using System.Drawing;
using System.Collections.Generic;

namespace PR5
{
    class TaulaOrdenable<T> where T : IOrdenable
    {
        public List<T> Taula { get; set; }
        public int numElements = 0;
        public TaulaOrdenable()
        {
            Taula = new List<T>();
        }
        public int Capacitat()
        {
            return Taula.Capacity;
        }
        public int NrElements()
        {
            return Taula.Count;
        }
        public int Afegir(T obj)
        {
            if (obj == null)
            {
                Console.WriteLine(" S'ha passat obj amb valor null - No es pot afegir");
                return -1;
            }
            else if (numElements + 1 == 50)
            {
                Console.WriteLine("No hi ha més espai");
                return -2;
            }
            else
            {
                Console.WriteLine($"{obj}\nS'ha afegit sense cap problema\n");
                Taula.Add(obj);
                numElements++;
                return 0;
            }
        }
        public object ExemplarAt(int pos)
        {
            if (pos > numElements || pos < 0)
            {
                Console.WriteLine("No existeix");
                return null;
            }
            return Taula[pos];
        }
        public object ExtreureAt(int pos)
        {
            if (ExemplarAt(pos)== null || pos < 0)
            {
                Console.WriteLine("No es pot eliminar, ja que no existeix.");
                return null;
            }
            else
            {
                object obj = ExemplarAt(pos);
                Taula.RemoveAt(pos);
                numElements--;
                Console.WriteLine("Eliminat correctament");
                return obj;
            }
        }
        public void Buidar()
        {
            Taula.Clear();
        }
        public T GetElemento(int i)
        {
            return Taula[i];
        }
        public void Visualtzar()
        {
            Console.WriteLine($"Capacitat: {Capacitat()}\nElements: {NrElements()}");
            Console.WriteLine();
            foreach (T item in Taula) Console.WriteLine(item);

        }
        public void Ordenar()
        {
            for (int i = 0; i < Taula.Count - 1; i++)
            {
                for (int j = 0; j < Taula.Count - i - 1; j++)
                {
                    if (Taula[j].Comparar(Taula[j + 1]) == -1) (Taula[j + 1], Taula[j]) = (Taula[j], Taula[j + 1]);
                }
            }
        }
    }
}
