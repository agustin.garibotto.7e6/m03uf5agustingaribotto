﻿using System;
using System.Drawing;

namespace PR5
{
    class Rectangle03 : FiguraGeometrica03, IOrdenable
    {
        // Atributos
        public double Base { get; set; }
        public double Alçada { get; set; }

        // Constructores
        public Rectangle03()
        {
            Base = 3.4;
            Alçada = 3.5;
        }
        public Rectangle03(int code, string name, Color color, double baseRect, double alçadaRect) : base(code, name, color)
        {
            Base = baseRect;
            Alçada = alçadaRect;
        }

        public Rectangle03(Rectangle03 copy) : base(copy)
        {
            Base = copy.Base;
            Alçada = copy.Alçada;
        }

        // Métodos
        public override double Area() { return Math.Round(Base * Alçada, 2); }
        private double Perimetre() { return Math.Round(2 * Base + 2 * Alçada, 2); }
        public override string ToString() { return $"{base.ToString()} Base [{Base}] Alçada[{Alçada}] Area[{Area()}] Perimetre[{Perimetre()}]"; }
    }
}
