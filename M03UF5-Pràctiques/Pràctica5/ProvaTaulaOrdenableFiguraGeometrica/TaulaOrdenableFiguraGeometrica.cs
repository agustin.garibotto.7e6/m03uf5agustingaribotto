﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PR5
{
    class TaulaOrdenableFiguraGeometrica : TaulaOrdenable<FiguraGeometrica03>
    {
        public TaulaOrdenableFiguraGeometrica(int z)
        {
            if (z < 0) Taula.Capacity = 10; 
            else
            {
                Taula.Capacity = z;
            }            
        }

    }
}
