﻿using System;
using System.Collections.Generic;
using System.Drawing;

namespace PR5
{
    class ProvaTaulaOrdenableFiguraGeometrica
    {
        public static void Main()
        {
            TaulaOrdenableFiguraGeometrica TaulaFigures = new TaulaOrdenableFiguraGeometrica(50);
            Console.WriteLine($"La capacitat de la taula és: {TaulaFigures.Capacitat()}\n"); 

            Rectangle03 rectangle = new Rectangle03(1, "Rectangle1", Color.Red, 3, 5);
            Cercle03 cercle1 = new Cercle03(2, "Cercle1", Color.Red, 5);
            Cercle03 cercle2 = new Cercle03(3, "Cercle2", Color.Yellow, 2);
            Triangle03 triangle1 = new Triangle03(4, "Triangle1", Color.Red, 3, 5);
            Triangle03 triangle2 = new Triangle03(5, "Triangle2", Color.Yellow, 2, 2);

            
            FiguraGeometrica03[] arrFigs = { rectangle, cercle1, cercle2, triangle1, triangle2 };

            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("AFEGIR");
            Console.ResetColor();
            TaulaFigures.Visualtzar();
            foreach (FiguraGeometrica03 fig in arrFigs) TaulaFigures.Afegir(fig); // Afegim a la taula les figures

            // VISUALITZAR
            Console.WriteLine("sense ordenar");
            Visualitzar(TaulaFigures);

            Console.WriteLine("\nordenada");
            TaulaFigures.Ordenar();
            Visualitzar(TaulaFigures);


            // PROVA EXEMPLAR AT
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("\nEXEMPLAR AT");
            Console.ResetColor();

            Console.WriteLine("\nL'exemplar en la posició 3\n");
            Console.Write($"{TaulaFigures.ExemplarAt(3)}\n"); 
            Console.WriteLine("\nL'exemplar en la posició -1\n");
            Console.Write($"{TaulaFigures.ExemplarAt(-1)}");


            // PROVA EXTREURE AT
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("\nEXTREURE AT");
            Console.ResetColor();
            Console.WriteLine("\nTreurem l'exemplar en la posició 4");
            TaulaFigures.ExtreureAt(4);
            Console.WriteLine("\nVisualitzem la taula sense l'exemplar en posició 4\n");
            Visualitzar(TaulaFigures);


            Console.WriteLine("\nIntentem eliminar l'exemplar en la posició 6 (spoiler, no existeix)\n");
            TaulaFigures.ExtreureAt(6);
            /*
             a. Cal crear una taula TaulaOrdenableFiguraGeometrica amb capacitat de guardar 50 elements.
            Generar un rectangle, dos cercles i dos triangles i afegiu-los a la taula, visualitza el resultat.
            Ordenar la taula i visualitzar-la. Prova els mètodes ExemplarAt i ExtreureAt i Buidar.
            */
        }


        public static void Visualitzar(TaulaOrdenableFiguraGeometrica TaulaFigures)
        {
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("VISUALITZAR");
            Console.ResetColor();
            TaulaFigures.Visualtzar();
        }

    }
}
