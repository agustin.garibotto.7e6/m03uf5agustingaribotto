﻿using System;
using System.Drawing;
using System.Collections.Generic;

namespace PR5
{
    abstract class FiguraGeometrica03 : IOrdenable
    {
        //Atributos
        public int Code;
        public string Name;
        public Color Color;

        //Constructores
        public FiguraGeometrica03()
        {
            Code = 0;
            Name = "DefaultFigure";
            Color = Color.Transparent;
        }
        public FiguraGeometrica03(int code, string name, Color color)
        {
            Code = code;
            Name = name;
            Color = color;

        }
        public FiguraGeometrica03(FiguraGeometrica03 copy)
        {
            Code = copy.Code;
            Name = copy.Name;
            Color = copy.Color;
        }

        //Métodos
        public abstract double Area();
        public override string ToString() { return $"Code [{Code}], Nom [{Name}], {Color}"; }
        public override int GetHashCode()
        {
            return Code; 
        }
        public override bool Equals(Object obj)
        {
            FiguraGeometrica03 figure = (FiguraGeometrica03)obj;
            if (figure.Code == Code)
            {
                return true;
            }
            else return false;
        }
        public int Comparar(IOrdenable figToCompare)
        {
            FiguraGeometrica03 figure = (FiguraGeometrica03)figToCompare;
            if (Area() == figure.Area()) return 0;
            else if (Area() > figure.Area()) return 1;
            else return -1;
        }

    }
}
