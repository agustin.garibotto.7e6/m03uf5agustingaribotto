﻿using System;

namespace PR5
{
    class Data02 : IOrdenable
    {
        public int Dia { get; set; }
        public int Mes { get; set; }
        public int Any { get; set; }

        private readonly int[] meses = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12 };
        private readonly int[] diasMes = { 1, -1, 1, 0, 1, 0, 1, 1, 0, 1, 0, 1 }; // 1 - 31 , -1 - 28/29 , 0 - 30

        public Data02(int dia, int mes, int any)
        {
            if (any < 0 || mes <= 0 || mes > meses.Length || dia < 0) // Primero comprovar que los datos de mes sean correctos para que no haya index out of bounds
            {
                Dia = 1;
                Mes = 1;
                Any = 1980;
            }
            else if (diasMes[mes - 1] == 1 && dia > 31 || diasMes[mes - 1] == -1 && (dia > 29) || diasMes[mes - 1] == 0 && dia > 30)
            {
                Dia = 1;
                Mes = 1;
                Any = 1980;
            }
            else
            {
                Dia = dia;
                Mes = mes;
                Any = any;
            }
        }
        public Data02(Data02 copy)
        {
            Dia = copy.Dia;
            Mes = copy.Mes;
            Any = copy.Any;
        }
        public Data02()
        {
            Dia = 1;
            Mes = 1;
            Any = 1980;
        }

        private void Resta(Data02 date, int value)
        {
            for (int i = 0; i < value * -1; i++)
            {
                if (date.Dia == 1)
                {
                    if (date.Mes == 1)
                    {
                        date.Dia = 31;
                        date.Mes = 12;
                        date.Any--;
                    }
                    else if (date.diasMes[date.Mes - 2] == 1) // Si el mes anterior tiene 31 dias
                    {
                        date.Dia = 31;
                        date.Mes--;
                    }
                    else if (date.diasMes[date.Mes - 2] == 0) // Si el mes anterior tiene 30 dias
                    {
                        date.Dia = 30;
                        date.Mes--;
                    }
                    else if (date.diasMes[date.Mes - 2] == -1) // Si el mes anterior tiene 28/29 dias
                    {
                        date.Dia = 28;
                        date.Mes--;
                    }
                }
                else date.Dia--;
            }
        }
        public void Sumar(int value, Data02 date) // Método para sumarle dias a una fecha
        {
            if (value > 0) // Suma
            {
                for (int i = 0; i < value; i++)
                {
                    if (date.diasMes[date.Mes - 1] == 1) // Si el mes tiene 31 dias
                    {
                        if (date.Dia == 31) PasarDeUnMesAOtroSuma(date);// Si el dia es 31, volvemos a 0 por así decirlo sumamos 1 y de 31 pasa a 0
                        else date.Dia++; // Si el dia es cualquier otro dia sumamos 1
                    }
                    else if (date.diasMes[date.Mes - 1] == -1) // Si el mes tiene 28 o 29 dias
                    {
                        if (date.Any % 4 == 0) // Si es año visiesto
                        {
                            if (date.Dia == 29) PasarDeUnMesAOtroSuma(date);
                            else date.Dia++;
                        }
                        else
                        {
                            if (date.Dia == 28) PasarDeUnMesAOtroSuma(date);
                            else date.Dia++;
                        }
                    }
                    else if (date.diasMes[date.Mes - 1] == 0) // Si el mes tiene 30 dias
                    {
                        if (date.Dia == 30) PasarDeUnMesAOtroSuma(date);
                        else date.Dia++;
                    }
                }
            }
        }

        private void PasarDeUnMesAOtroSuma(Data02 date)
        {
            date.Dia = 1;
            if (date.Mes == 12)
            {
                date.Mes = 1;
                date.Any++;
            }
            else date.Mes++;
        }
        public Data02 SumarRestar(int value, Data02 copy)
        {
            Data02 date = new Data02(copy);

            if (value < 0) // Resta
            {
                Resta(date, value);
            }
            else if (value > 0) // Suma
            {
                Sumar(value, date);
            }
            return date;
        }

        public int NumDiesDiff(Data02 date2)
        {
            int dias = 0;
            while (Any < date2.Any || (Any == date2.Any && Mes < date2.Mes) || (Any == date2.Any && Mes == date2.Mes && Dia < date2.Dia))
            {
                Sumar(1, this); // Le pasamos el objecto que llama a la función
                dias++;
            }
            return dias;
        }
       
        public int Comparar(IOrdenable dateToCompare)
        {
            Data02 data = (Data02)dateToCompare;

            if (Any > data.Any) return 1;
            else if (Any == data.Any)
            {
                if (Mes > data.Mes) return 1;

                else if (Mes == data.Mes)
                {
                    if (Dia > data.Dia) return 1;
                    else if (Dia == data.Dia) return 0;
                    else return -1;
                }
                else return -1;
            }
            else return -1;
        }

        public void CompareDates(Data02 dateToCompare)
        {
            if (Any > dateToCompare.Any)
            {
                Console.WriteLine(ToString() + " > " + dateToCompare.ToString());
            }
            else if (Any == dateToCompare.Any)
            {
                if (Mes > dateToCompare.Mes) Console.WriteLine(ToString() + " > " + dateToCompare.ToString());
                else if (Mes == dateToCompare.Mes)
                {
                    if (Dia > dateToCompare.Dia) Console.WriteLine(ToString() + " > " + dateToCompare.ToString());
                    else if (Dia == dateToCompare.Dia) Console.WriteLine(ToString() + " = " + dateToCompare.ToString());
                    else Console.WriteLine(ToString() + " < " + dateToCompare.ToString());
                }
                else Console.WriteLine(ToString() + " < " + dateToCompare.ToString());
            }
            else Console.WriteLine(ToString() + " < " + dateToCompare.ToString());
        }
        public override string ToString()
        {
            return $"{Dia}/{Mes}/{Any}";
        }

    }
}
