﻿using System;
using System.Drawing;

namespace PR5
{
    class Triangle03 : FiguraGeometrica03, IOrdenable
    {
        // Atributos
        public double Base { get; set; }
        public double Alçada { get; set; }

        // Constructores
        public Triangle03()
        {
            Base = 5.6;
            Alçada = 10.3;
        }
        public Triangle03(int code, string name, Color color, double baseTriangle, double alçadaTriangle) : base(code, name, color)
        {
            Base = baseTriangle;
            Alçada = alçadaTriangle;
        }
        public Triangle03(Triangle03 copy) : base(copy)
        {
            Base = copy.Base;
            Alçada = copy.Alçada;
        }

        //Métodos
        public override double Area()
        {
            return Math.Round(Base * Alçada / 2, 2);
        }
        public override string ToString() { return $"{base.ToString()} Alçada[{Alçada}] Area[{Area()}]"; }

    }
}
