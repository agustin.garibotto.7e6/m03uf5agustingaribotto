﻿using System;
using System.Drawing;

namespace PR5
{  
    class Cercle03 : FiguraGeometrica03, IOrdenable
    {
        // Atributos
        public double Radi { get; set; }
        public Cercle03()
        {
            Radi = 5.3;
        }

        // Constructores
        public Cercle03(int code, string name, Color color, double radi) : base(code, name, color)
        {
            Radi = radi;
        }
        public Cercle03(Cercle03 copy) : base(copy)
        {
            Radi = copy.Radi;
        }

        // Métodos
        public override double Area()
        {
            return Math.Round(Math.PI * Math.Pow(Radi, 2), 2);
        }
        public double Perimetre()
        {
            return Math.Round(2 * Radi * Math.PI, 2);
        }
        public override string ToString() { return $"{base.ToString()}  Area[{Area()}] Perimetre[{Perimetre()}]"; }
    }
}
