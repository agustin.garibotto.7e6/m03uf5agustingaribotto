﻿
namespace TrexEndlessRunner
{
    partial class DinoRunner
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.dino = new System.Windows.Forms.PictureBox();
            this.cactus1 = new System.Windows.Forms.PictureBox();
            this.cactus2 = new System.Windows.Forms.PictureBox();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.GameTimer = new System.Windows.Forms.Timer(this.components);
            this.Puntuacion = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dino)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cactus1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cactus2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            this.SuspendLayout();
            // 
            // dino
            // 
            this.dino.Image = global::TrexEndlessRunner.Properties.Resources.running;
            this.dino.Location = new System.Drawing.Point(57, 360);
            this.dino.Name = "dino";
            this.dino.Size = new System.Drawing.Size(40, 43);
            this.dino.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.dino.TabIndex = 0;
            this.dino.TabStop = false;
            // 
            // cactus1
            // 
            this.cactus1.Image = global::TrexEndlessRunner.Properties.Resources.obstacle_1;
            this.cactus1.Location = new System.Drawing.Point(431, 358);
            this.cactus1.Name = "cactus1";
            this.cactus1.Size = new System.Drawing.Size(23, 46);
            this.cactus1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.cactus1.TabIndex = 0;
            this.cactus1.TabStop = false;
            this.cactus1.Tag = "obstacle";
            // 
            // cactus2
            // 
            this.cactus2.Image = global::TrexEndlessRunner.Properties.Resources.obstacle_2;
            this.cactus2.Location = new System.Drawing.Point(517, 371);
            this.cactus2.Name = "cactus2";
            this.cactus2.Size = new System.Drawing.Size(32, 33);
            this.cactus2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.cactus2.TabIndex = 0;
            this.cactus2.TabStop = false;
            this.cactus2.Tag = "obstacle";
            // 
            // pictureBox4
            // 
            this.pictureBox4.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.pictureBox4.Location = new System.Drawing.Point(0, 403);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(697, 50);
            this.pictureBox4.TabIndex = 1;
            this.pictureBox4.TabStop = false;
            // 
            // GameTimer
            // 
            this.GameTimer.Interval = 20;
            this.GameTimer.Tick += new System.EventHandler(this.GameEventTick);
            // 
            // Puntuacion
            // 
            this.Puntuacion.AutoSize = true;
            this.Puntuacion.Font = new System.Drawing.Font("Stencil", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.Puntuacion.Location = new System.Drawing.Point(57, 31);
            this.Puntuacion.Name = "Puntuacion";
            this.Puntuacion.Size = new System.Drawing.Size(126, 19);
            this.Puntuacion.TabIndex = 2;
            this.Puntuacion.Text = "Puntuación: 0";
            // 
            // DinoRunner
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(697, 450);
            this.Controls.Add(this.Puntuacion);
            this.Controls.Add(this.dino);
            this.Controls.Add(this.pictureBox4);
            this.Controls.Add(this.cactus2);
            this.Controls.Add(this.cactus1);
            this.Name = "DinoRunner";
            this.Text = "DinoRunner";
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TeclaPulsada);
            this.KeyUp += new System.Windows.Forms.KeyEventHandler(this.TeclaSoltada);
            ((System.ComponentModel.ISupportInitialize)(this.dino)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cactus1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cactus2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox dino;
        private System.Windows.Forms.PictureBox cactus1;
        private System.Windows.Forms.PictureBox cactus2;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.Timer GameTimer;
        private System.Windows.Forms.Label Puntuacion;
    }
}

