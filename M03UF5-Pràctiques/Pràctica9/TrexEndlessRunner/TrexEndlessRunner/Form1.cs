﻿using System;
using System.Windows.Forms;

// 
namespace TrexEndlessRunner
{
    public partial class DinoRunner : Form
    {
        bool saltando = false;
        int velocidadSalto;
        int fuerzaSalto = 12;
        int puntos = 0;
        int velocidadObstaculo = 10;
        readonly Random rnd = new Random();
        int posicion;
        bool isGameOver = false;
        bool tocaSuelo = true;
        public DinoRunner()
        {
            InitializeComponent();
            ReiniciarJuego();
        }

        private void TeclaPulsada(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Space && !saltando && tocaSuelo == true)
            {
                tocaSuelo = false;
                saltando = true;
            }
        }

        private void TeclaSoltada(object sender, KeyEventArgs e)
        {
            
            saltando = false;

            // Si pierde la partida y pulsa la R, reiniciamos la partida.
            if (e.KeyCode == Keys.R && isGameOver == true) ReiniciarJuego();
        }

        private void GameEventTick(object sender, EventArgs e)
        {
            // Cada tick actualizamos el dinosaurio , el score y los cactus
            ComprobarEstadoDinosaurio();
            ActualizarPuntuacion();
            ComprobarEstadoCactus();
        }
        private void ComprobarEstadoCactus()
        {
            // Por cada componente 
            foreach (Control x in this.Controls)
            {
                // Si el componente es un cactus, se moverá hacia la izquierda cada tick.
                if (x is PictureBox && (string)x.Tag == "obstacle")
                {
                    x.Left -= velocidadObstaculo;

                    // En caso de llegar fuera de la pantalla.
                    if (x.Left < -100)
                    {
                        // Lo traspasaremos a la derecha mediante una posición aleatoria, para que cada vez que vengan estén en posiciones diferentes y también aumentaremos en 1 la puntuación
                        x.Left = this.ClientSize.Width + rnd.Next(200, 500) + (x.Width * 15);
                        puntos++;
                    }

                    // Si los límites de un cactus intersecta contra los límites del dinosaurio, paramos el juego y cambiamos la foto del dinosaurio y el texto en la puntuación
                    if (dino.Bounds.IntersectsWith(x.Bounds))
                    {
                        GameTimer.Stop();
                        dino.Image = Properties.Resources.dead;
                        Puntuacion.Text += " Pulsa R para reiniciar el juego!";
                        isGameOver = true;
                    }
                    
                }
            }

        }
        
        private void ActualizarPuntuacion()
        {
            Puntuacion.Text = "Puntuación: " + puntos;

            if (puntos > 5) velocidadObstaculo = 14; // Cuando llegue a 6 puntos aumentamos la dificultad (velocidad)

            if (puntos > 10) velocidadObstaculo = 17; // Cuando llegue a 10 puntos aumentamos la dificultad (velocidad)
        }
        private void ComprobarEstadoDinosaurio()
        {
            dino.Top += velocidadSalto;
            // Si se ha acabado la fuerza del salto, empezamos a caer
            if (saltando && fuerzaSalto < 0) saltando = false;

            // Si está saltando, aumentamos la altura del dinosaurio y vamos bajando la fuerza para que llegue a 0 y deje de subir
            if (saltando)
            {

                velocidadSalto = -12;
                fuerzaSalto -= 1;
            }
            else velocidadSalto = 12; // Si no está saltando, reseteamos la velocidad

            // Si el dinosaurio está a punto de caer por debajo de la posición por defecto y no está saltando, reseteamos la posición del dinosaurio, la fuerza y la velocidad del salto.
            if (dino.Top > 359 && !saltando)
            {
                fuerzaSalto = 10;
                dino.Top = 360;
                velocidadSalto = 0;
                tocaSuelo = true;
            }
        }
        private void ReiniciarJuego()
        {
            // Reiniciamos las variables.
            fuerzaSalto = 12;
            velocidadSalto = 0;
            saltando = false;
            puntos = 0;
            velocidadObstaculo = 10;
            Puntuacion.Text = "Puntuación: " + puntos;
            dino.Image = Properties.Resources.running;
            isGameOver = false;
            dino.Top = 360;

            
            foreach (Control x in this.Controls)
            {
                // Generamos los cactus en una posición a la derecha aleatoria.
                if(x is PictureBox && (string)x.Tag == "obstacle")
                {
                    posicion = this.ClientSize.Width + rnd.Next(500, 800) + (x.Width * 10);
                    x.Left = posicion;
                }
            }
            GameTimer.Start();

        }
    }
}
