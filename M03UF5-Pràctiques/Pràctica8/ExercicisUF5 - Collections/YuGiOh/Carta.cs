﻿using System;
using System.Collections.Generic;
using System.Text;

namespace YuGiOh
{
    class Carta
    {
        public string Nombre { get; set; }
        public string Tipo { get; set; }
        public int Nivel { get; set; }
        public string Efecto { get; set; }

        public Carta(string nombre, string tipo, int nivel, string efecto)
        {
            Nombre = nombre;
            Tipo = tipo;
            Nivel = nivel;
            Efecto = efecto;
        }
        public override string ToString()
        {
            return $"{Nombre}, {Tipo}, {Nivel}, {Efecto}";
        }
    }
}
