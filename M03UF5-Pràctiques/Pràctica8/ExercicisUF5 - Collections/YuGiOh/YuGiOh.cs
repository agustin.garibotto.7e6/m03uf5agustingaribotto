﻿using System;

namespace YuGiOh
{
    internal class YuGiOh
    {
        static void Main()
        {
            MazoCartas mazo = new MazoCartas();
            Carta dragonBlancoOjosAzules = new Carta("Dragon Blanco de Ojos Azules", "Monstruo", 8, "Este legendario dragón es un poderoso motor de destrucción. Virtualmente invencible, muy pocos se han enfrentado a esta impresionante criatura y han vivido para contarlo.");
            Carta agujeroOscuro = new Carta("Agujero Oscuro", "Hechizo", 0, "Destruye todos los monstruos en el campo.");
            Carta dragonMaterialOscuro = new Carta("Dragón de Material Oscuro", "Monstruo", 8, "Este dragón puede ser convocado de forma especial sacrificando 1 monstruo de oscuridad y 1 monstruo dragón.");
            Carta MagoOscuro = new Carta("Mago Oscuro", "Lanzador de conjuros", 7, "Atk/2500 Def/2100");
            
            ManoJugador manoJugador1 = new ManoJugador();
            ManoJugador manoJugador2 = new ManoJugador();

            // Añadimos algunas cartas al mazo
            Console.WriteLine("Creamos un mazo");
            Console.WriteLine($"El mazo está vacío? {mazo.EstaVacio()}");

            Console.WriteLine("\nAñadimos 3 cartas al mazo");
            mazo.AgregarCarta(dragonBlancoOjosAzules);
            mazo.AgregarCarta(agujeroOscuro);
            mazo.AgregarCarta(dragonMaterialOscuro);
            Console.WriteLine($"El mazo está vacío? {mazo.EstaVacio()}\n");

            // Creamos una mano de jugador vacía
            Console.WriteLine("Creamos una mano para jugador1");
            

            //Robamos una carta del mazo (Debería robar Dragón de material Oscuro)
            Console.WriteLine("Jugador1 roba una carta");
            manoJugador1.RobarCarta(mazo);
            Console.WriteLine($"La mano del jugador1 está vacía? {manoJugador1.EstaVacia()}");


            //Descartamos carta de la mano (Si no tiene dicha carta en la mano no se lo permitirá)
            Console.WriteLine("\nJugador1 descarta una carta\n");
            manoJugador1.DescartarCarta(dragonBlancoOjosAzules); // No la tiene en el mazo
            manoJugador1.DescartarCarta(dragonMaterialOscuro); // Si la tiene en el mazo
            Console.WriteLine($"La mano del jugador1 está vacía? {manoJugador1.EstaVacia()}");


            mazo.AgregarCarta(MagoOscuro); // Comprovamos que el último en entrar es el primero que sale
            //Agregamos una carta a jugador1 y otra al otro jugador2
            Console.WriteLine("\nJugador1 y jugador2 roban una carta\n");
            manoJugador2.RobarCarta(mazo); // Roba Mago Oscuro
            Console.WriteLine($"El mazo está vacío? {mazo.EstaVacio()}");
            manoJugador1.RobarCarta(mazo);
            Console.WriteLine($"El mazo está vacío? {mazo.EstaVacio()}");
            manoJugador2.RobarCarta(mazo); // Probamos a robar una carta para comprovar que pasa si el mazo está vacíó


            // Agregamos una carta más al mazo
            


            //Descartamos las cartas del jugador1 para comprovar si sigue vivo
            Console.WriteLine("\nJugador1 descarta sus cartas");
            manoJugador1.DescartarCarta(MagoOscuro);
            Console.WriteLine($"jugador1: {manoJugador1.JugadorVivo}");
            Console.WriteLine($"jugador2: {manoJugador2.JugadorVivo}");


        }
    }
}
