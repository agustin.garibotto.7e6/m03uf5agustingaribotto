﻿using System;
using System.Collections;
using System.Text;
using System.Collections.Generic;

namespace YuGiOh
{
    class MazoCartas : Stack<Carta>
    {
        Stack Stack { get; set; }
        public MazoCartas()
        {
           Stack = new Stack();
        }

        public void AgregarCarta(Carta carta)
        {
            Stack.Push(carta);
        }
        public Carta RobarCarta()
        {
            if (EstaVacio()) return null;
            else return (Carta)Stack.Pop();
        }
        public bool EstaVacio()
        {
            return Stack.Count == 0;
        }
    }
}
