﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace YuGiOh
{
    class ManoJugador
    {
        public List<Carta> CartaList { get; set; }
        public string jugadorVivo;

        public string JugadorVivo
        {
            get
            {
                return EstaVacia()  ? "muerto" : "vivo";
            }
        }
    
        public ManoJugador()
        {
            CartaList = new List<Carta>();
        }
        public void JugarCarta(Carta carta)
        {
            Console.WriteLine($"El jugador juega la carta {carta}");
            CartaList.RemoveAt(CartaList.IndexOf(carta));
        }
        public void DescartarCarta(Carta carta)
        {
            if(!CartaList.Contains(carta)) Console.WriteLine("No se puede descartar la carta ya que no la tiene en la mano");
            else
            {
                Console.WriteLine($"El jugador descarta la carta {carta}");
                CartaList.RemoveAt(CartaList.IndexOf(carta));
            }
              
        }
        public bool EstaVacia()
        {
            return CartaList.Count == 0;
        }
        public void RobarCarta(MazoCartas mazo)
        {
            Carta carta = mazo.RobarCarta();
            if (carta != null)
            {
                Console.WriteLine($"Robando la carta {carta.Nombre}");
                CartaList.Add(carta);
            }
            else Console.WriteLine("No se puede robar una carta porque el mazo está vacío");

        }
    }
}
