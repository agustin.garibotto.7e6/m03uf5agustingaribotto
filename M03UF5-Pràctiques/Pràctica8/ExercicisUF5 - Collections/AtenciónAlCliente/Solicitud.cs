﻿namespace AtenciónAlCliente
{
    class Solicitud
    {
        public string NombreCliente { get; set; }
        public string TipoSolicitud { get; set; }

        public Solicitud(string nombreCliente, string tipoSolicitud)
        {
            NombreCliente = nombreCliente;
            TipoSolicitud = tipoSolicitud;
        }

        public override string ToString()
        {
            return $"{NombreCliente},{TipoSolicitud}";
        }
    }
}
