﻿using System;
using System.Collections;

namespace AtenciónAlCliente
{
    class ColaCliente : Queue
    {
        public Queue Cola { get; set; }
        public ColaCliente()
        {
            Cola = new Queue();
        }

        // Métodos
        public void AgregarSolicitud(string nombreCliente, string tipoSolicitud)
        {
            Solicitud solicitud = new Solicitud(nombreCliente, tipoSolicitud);
            Cola.Enqueue(solicitud);
        }
        public void MostrarSiguienteSolicitud()
        {
            object[] solis = Cola.ToArray();
            if (solis.Length == 0) Console.WriteLine("No hay solicitudes pendientes");
            else
            {
                Solicitud siguienteSolicitud = (Solicitud)solis[0];
                Console.WriteLine(siguienteSolicitud);
            }
        }
        public void EliminarSolicitudAtendida()
        {
            object[] solis = Cola.ToArray();
            if (solis.Length == 0) Console.WriteLine("No hay ninguna solicitud por confirmar");
            else Cola.Dequeue();
        }
        public int SolicitudesPendientes()
        {
            return Cola.Count;
        }
    }
}
