﻿using System;

namespace AtenciónAlCliente
{
    class AtenciónAlCliente
    {
        private static readonly ColaCliente Cola = new ColaCliente();
        static void Main()
        {
            Menu();
        }
        static void Menu()
        {
            string opcio;
           
            do
            {
                Console.WriteLine("1. Ingresar solicitud\n2. Mostrar siguiente solicitud\n3. Confirmar siguiente solicitud\n0. Dejar de enviar solicitudes");
                opcio = Console.ReadLine();
                switch (opcio)
                {
                    case "1":
                        Cola.AgregarSolicitud(DemanarNom(), DemanarSolicitud());
                        Cola.MostrarSiguienteSolicitud();
                        break;
                    case "2":
                        Cola.MostrarSiguienteSolicitud();
                        break;
                    case "3":
                        Cola.EliminarSolicitudAtendida();
                        break;
                    case "0":
                        break;
                    default:
                        Console.WriteLine("Opció incorrecta");
                        break;
                }
                Console.WriteLine("Solicitudes pendientes: " + Cola.SolicitudesPendientes());
                Console.Write("Pulsa cualquier tecla...");
                Console.ReadKey();
                Console.Clear();

            } while (opcio!="0");
        }
        
        public static string DemanarNom()
        {
            Console.Write("Introdueix el nom del client:");
            return Console.ReadLine();
        }
        public static string DemanarSolicitud()
        {
            Console.Write("Introdueix la solicitud:");
            return Console.ReadLine();
        }
    }
}
