﻿using System;
using System.Collections;
using System.Text;
using System.Collections.Generic;

namespace JustFeed
{
    class ColaReparto : Queue
    {
        public Queue Cola { get; set; }
        public ColaReparto()
        {
            Cola = new Queue();
        }
        public void AgregarPedido(string nombre, string dir, List<string> platos)
        {
            Cola.Enqueue(new Pedido(nombre, dir, platos));
        }
        public void MostrarProximoPedido()
        {
            object[] solis = Cola.ToArray();
            if (solis.Length == 0) Console.WriteLine("No hay pedidos pendientes");
            else
            {
                Pedido siguienteSolicitud = (Pedido)solis[0];
                Console.WriteLine(siguienteSolicitud);
            }
        }
        public void EliminarProximoPedido()
        {
            object[] solis = Cola.ToArray();
            if (solis.Length == 0) Console.WriteLine("No hay ninguna solicitud por confirmar");
            else Cola.Dequeue();
        }
    }
}
