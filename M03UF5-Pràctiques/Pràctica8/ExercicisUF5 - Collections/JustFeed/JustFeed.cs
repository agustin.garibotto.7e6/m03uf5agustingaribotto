﻿using System;
using System.Collections.Generic;

namespace JustFeed
{
    class JustFeed
    {
        private static readonly ColaReparto Cola = new ColaReparto();
        static void Main()
        {
            Menu();
        }
        static void Menu()
        {
            string opcio;

            do
            {
                Console.WriteLine("1. Ingresar pedido\n2. Mostrar siguiente pedido\n3. Confirmar siguiente pedido\n0. Dejar de enviar pedidos");
                opcio = Console.ReadLine();
                switch (opcio)
                {
                    case "1":
                        string nom = DemanarNom();
                        string dir = DemanarDireccio();
                        List<string> list = DemanarPlats();
                        if (list.Count == 0) Console.WriteLine("Pedido no añadido, ya que no quiere ningún plato"); 
                        else Cola.AgregarPedido(nom, dir, list);
                        Cola.MostrarProximoPedido();
                        break;
                    case "2":
                        Cola.MostrarProximoPedido();
                        break;
                    case "3":
                        Cola.EliminarProximoPedido();
                        break;
                    case "0":
                        break;
                    default:
                        Console.WriteLine("Opció incorrecta");
                        break;
                }
                Console.Write("Pulsa cualquier tecla...");
                Console.ReadKey();
                Console.Clear();

            } while (opcio != "0");
        }
        public static string DemanarNom()
        {
            Console.Write("Introdueix el nom del client:");
            return Console.ReadLine();
        }
        public static string DemanarDireccio()
        {
            Console.Write("Introdueix la teva direcció:");
            return Console.ReadLine();
        }
        public static List<string> DemanarPlats()
        {
            List<string> list = new List<string>();
            string opcio;
            do
            {
                Console.WriteLine("Introdueix el número del plat que vulguis:\n1. Hamburguesa\n2. Milanesa\n3. Ensalada\n4. Spaguetti\n0. Finalizar pedido.");
                opcio = Console.ReadLine();
                switch (opcio)
                {
                    case "1":
                        list.Add("Hamburguesa");
                        break;
                    case "2":
                        list.Add("Milanesa");
                        break;
                    case "3":
                        list.Add("Ensalada");
                        break;
                    case "4":
                        list.Add("Spaguetti");
                        break;
                    case "0":
                        break;
                    default:
                        Console.WriteLine("Opció incorrecta");
                        break;
                }
                if (opcio != "0") Console.WriteLine("Añadido correctamente");
                Console.Write("Pulsa cualquier tecla para continuar...");
                Console.ReadKey();
                Console.Clear();
            } while (opcio != "0");
            return list;

        }
    }
}
