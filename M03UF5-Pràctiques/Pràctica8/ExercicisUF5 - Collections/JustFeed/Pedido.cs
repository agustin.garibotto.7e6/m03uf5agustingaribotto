﻿using System;
using System.Collections.Generic;
using System.Text;

namespace JustFeed
{
    class Pedido
    {
        public string Nombre { get; set; }  
        public string Dirección { get; set; }
        public List<string> Platos { get; set; }

        public Pedido(string nombre, string dirección, List<string> platos)
        {
            Nombre = nombre;
            Dirección = dirección;
            Platos = platos;
        }

        public override string ToString()
        {
            string platos = "";
            foreach (string plato in Platos) platos += plato + " ";
            return $"Pedido de {Nombre}: {Dirección}, {platos}";
        }

    }
}
