﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace AgustínGaribottoP6M03UF5
{
    class ExercicisUF5
    {
        static void Main()
        {
            ExercicisUF5 exercicisUF5 = new ExercicisUF5();
            exercicisUF5.Menu();
        }
        public void Menu()
        {
            string opció;
            do
            {
                Console.WriteLine("Introdueix una opció");
                Console.WriteLine("1. Exercici 6.1\n2. Exercici 6.2\n3. Exercici 6.3\n4. Exercici 6.4\n0. Sortir del programa");
                opció = Console.ReadLine();
                switch (opció)
                {
                    case "1":
                        Exercici6_1();
                        break;
                    case "2":
                        Exercici6_2();
                        break;
                    case "3":
                        Exercici6_3();
                        break;
                    case "4":
                        Exercici6_4();
                        break;
                    case "0":
                        break;
                    default:
                        Console.WriteLine("Opció Incorrecta");
                        break;
                }
                Console.Write("Prem cualsevol tecla per sortir...");
                Console.ReadKey();
                Console.Clear();
            }
            while (opció != "0");


        }
        // EXERCICI 1
        public static void Exercici6_1()
        {
            Console.WriteLine(" -  EXERCICI 6.1 - ");
            Console.WriteLine($"7 / 2 = {DivideixoCero(7, 2)}");
            Console.WriteLine($"8 / 4 = {DivideixoCero(8, 4)}");
            Console.WriteLine($"5 / 0 = {DivideixoCero(5, 0)}");

        }
        public static int DivideixoCero(int dividend, int divisor)
        {
            int resultat;
            try
            {
                resultat = dividend / divisor;
            }
            catch (ArithmeticException)
            {
                resultat = 0;
            }
            return resultat;
        }
        // EXERCICI 2
        public static void Exercici6_2()
        {
            Console.WriteLine(" -  EXERCICI 6.2 - ");
            Console.WriteLine($"7.1 = {ADoubleU(7.1)}");
            Console.WriteLine($"9. = {ADoubleU("9.")}");
            Console.WriteLine($".2 = {ADoubleU(.2)}");
            Console.WriteLine($"tres = {ADoubleU("tres")}");
        }
        public static double ADoubleU(object any)
        {
            try
            {
                return Convert.ToDouble(any);
            }
            catch (FormatException)
            {
                return 1.0;
            }
        }
        // EXERCICI 3
        public static void Exercici6_3()
        {
            Console.WriteLine(" -  EXERCICI 6.3 - ");
            Console.WriteLine($"fitxerExistent.txt = {LlegirFitxer("fitxerExistent.txt")}\n");
            Console.WriteLine($"fitxerNoExistent.txt = {LlegirFitxer("fitxerNoExistent.txt")}");

        }
        public static string LlegirFitxer(string fitxer)
        {
            string path = Path.GetFullPath(@"..\..\..\fitxers");
            string filePath = path + "\\" + fitxer;

            try
            {
                string s = File.ReadAllText(filePath);
                return s;

            }
            catch (IOException)
            {
                return "El sistema no puede encontrar el archivo especificado";
            }
        }
        // EXERCICI 4
        public static void Exercici6_4()
        {
            List<int> mutableList = new List<int>();
            mutableList.AddRange(Enumerable.Repeat(0, 10)); // añadimos 0 dieces

            string opció;
            do
            {
                Console.WriteLine("Que vols fer amb la llista? \n1. Afegir Null \n2. Afegir un número random a una posició determinada \n3. Afegir un número a la llista \n4. Mostrar elements de la llista \n5. Eliminar un element a una posició determinada \n0. Sortir de l'exercici");
                opció = Console.ReadLine();
                switch (opció)
                {
                    case "1":
                        string myArg = null;
                        try
                        {
                            AfegirNumToList(myArg, mutableList);
                        }
                        catch (ArgumentNullException)
                        {
                            Console.WriteLine("No pots afegir un valor null a una llista d'integers (en realidad si porque contaría como 0, pero no te dejo)");
                        }
                        break;
                    case "2":
                        Console.Write("Posició: ");
                        AfegirNumRandom(mutableList);
                        break;
                    case "3":
                        Console.Write("Num: ");

                        string myArg2 = Console.ReadLine();
                        try
                        {
                            AfegirNumToList(myArg2, mutableList);
                        }
                        catch (FormatException)
                        {
                            Console.WriteLine("Aquest número no és vàlid");
                        }
                        catch (ArgumentNullException)
                        {
                            Console.WriteLine("No pots afegir un valor null a una llista d'integers");
                        }
                        break;
                    case "4":
                        ShowList(mutableList);
                        break;
                    case "5":
                        Console.Write("Posició: ");
                      
                        try
                        {
                            DeletePosition(AskNum(), mutableList);
                        }
                        catch (ArgumentNullException)
                        {
                            Console.WriteLine("No pots afegir un valor null a una llista d'integer");
                        }

                        break;
                    case "0":
                        break;
                    default:
                        Console.WriteLine("Opció Incorrecta");
                        break;
                }
                Console.Write("Prem cualsevol tecla per sortir...");
                Console.ReadKey();
                Console.Clear();
            } while (opció != "0");





        }

        static int AskNum()
        {
            bool resultat;
            int number;
           
            do
            {
                resultat = int.TryParse(Console.ReadLine(), out number);
                if (!resultat) Console.WriteLine("Format incorrecte");
            } while (!resultat);
          
            return number;
        }
        static void DeletePosition(int arg, List<int> list)
        {

            try
            {
                int a = list[arg];
                list.RemoveAt(arg);
                Console.WriteLine($"{a} S'ha eliminat correctament");
            }
            catch (ArgumentOutOfRangeException)
            {

                Console.WriteLine("Aquesta posició no existeix a la llista");
            }
        }
        static void ShowList(List<int> list)
        {
            Console.WriteLine("Llista");
            foreach (int num in list)
            {
                Console.Write($"{num} ");
            }
            Console.WriteLine();
        }
        static void AfegirNumRandom(List<int> list)
        {
            Random rnd = new Random();
            try
            {
                int num = rnd.Next(0, 12345);
                list.Insert(Convert.ToInt32(Console.ReadLine()), num);
                Console.WriteLine($"{num} s'ha afegit correctament");
            }
            catch (ArgumentOutOfRangeException)
            {

                Console.WriteLine("Aquesta posició no existeix a la llista");
            }
        }
        static void AfegirNumToList(string arg, List<int> list)
        {
            if (arg == null)
            {
                throw new ArgumentNullException(nameof(arg)); // No se me ocurre otra forma de provocar un "Value cannot be null"
            }
            else
            {
                try
                {
                    int a = Convert.ToInt32(arg);
                    list.Add(a);
                    Console.WriteLine("Afegit correctament");
                }
                catch (FormatException)
                {
                    Console.WriteLine("Aquest número no és vàlid");
                }

            }
        }

    }
}



